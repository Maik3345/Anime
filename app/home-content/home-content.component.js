"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HomeContentComponent = /** @class */ (function () {
    function HomeContentComponent() {
    }
    HomeContentComponent.prototype.ngOnInit = function () { };
    HomeContentComponent = __decorate([
        core_1.Component({
            selector: "home-content",
            moduleId: module.id,
            templateUrl: "./home-content.component.html",
            styleUrls: ["./home-content.component.css"]
        }),
        __metadata("design:paramtypes", [])
    ], HomeContentComponent);
    return HomeContentComponent;
}());
exports.HomeContentComponent = HomeContentComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS1jb250ZW50LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImhvbWUtY29udGVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFTbEQ7SUFFQztJQUFnQixDQUFDO0lBRWpCLHVDQUFRLEdBQVIsY0FBYSxDQUFDO0lBSkYsb0JBQW9CO1FBUGhDLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsY0FBYztZQUN4QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLCtCQUErQjtZQUM1QyxTQUFTLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQztTQUMzQyxDQUFDOztPQUVXLG9CQUFvQixDQUtoQztJQUFELDJCQUFDO0NBQUEsQUFMRCxJQUtDO0FBTFksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcblx0c2VsZWN0b3I6IFwiaG9tZS1jb250ZW50XCIsXG5cdG1vZHVsZUlkOiBtb2R1bGUuaWQsXG5cdHRlbXBsYXRlVXJsOiBcIi4vaG9tZS1jb250ZW50LmNvbXBvbmVudC5odG1sXCIsXG5cdHN0eWxlVXJsczogW1wiLi9ob21lLWNvbnRlbnQuY29tcG9uZW50LmNzc1wiXVxufSlcblxuZXhwb3J0IGNsYXNzIEhvbWVDb250ZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuXHRjb25zdHJ1Y3RvcigpIHsgfVxuXG5cdG5nT25Jbml0KCkgeyB9XG59Il19