import { Component, OnInit } from "@angular/core";

@Component({
	selector: "home-content",
	moduleId: module.id,
	templateUrl: "./home-content.component.html",
	styleUrls: ["./home-content.component.css"]
})

export class HomeContentComponent implements OnInit {

	constructor() { }

	ngOnInit() { }
}