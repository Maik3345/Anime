import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { SharedModule } from "../shared/shared.module";
import { HomeContentRoutingModule } from "./home-content-routing.module";
import { HomeContentComponent } from "./home-content.component";

@NgModule({
    imports: [
        NativeScriptModule,
        HomeContentRoutingModule,
        SharedModule
    ],
    declarations: [
        HomeContentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HomeContentModule { }
