import { Component } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { PullToRefresh } from "nativescript-pulltorefresh";
import { Observable as RxObservable } from "rxjs/Observable";
import { registerElement } from "nativescript-angular/element-registry";
registerElement("pullToRefresh",() => require("nativescript-pulltorefresh").PullToRefresh);
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";

@Component({
    selector: 'home-list',
//     template: ` <SegmentedBar>
//     <SegmentedBarItem title="Hola">
//     </SegmentedBarItem>
//     <SegmentedBarItem title="Hola">
//     </SegmentedBarItem>
// </SegmentedBar>`
    templateUrl: "./home/home-list/home-list.html"
})
export class HomeListComponent {
    public notices: any = {
        articles: [
        ]
    };
    public temporalUrl: string = "";
    public title = "hola";

    constructor(public http: Http) {
        this.getNotices("https://newsapi.org/v1/articles?source=abc-news-au&sortBy=top&apiKey");
    }


    // https://newsapi.org/v1/articles?source=abc-news-au&sortBy=top&apiKey
    // metodo para consultar noticias
    getNotices(url,args?: any) {
        this.temporalUrl = url;
        // this.notices = [];
        let headers = new Headers();
        // set headers here e.g.
        headers.append("Content-Type", "application/json");
        this.http.get(`${url}=1b13ca21cd1742c78f34609b939503a2`, { headers: headers }).map(res => res.json()).subscribe(res => {
            this.notices = res;
            this.title = res.source;
            (<PullToRefresh>args.object).refreshing = false;
            console.log(this.notices.articles);
        }, error => {
            console.log(error);
        })
    }

}
