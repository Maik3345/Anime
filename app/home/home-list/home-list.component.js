"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var element_registry_1 = require("nativescript-angular/element-registry");
element_registry_1.registerElement("pullToRefresh", function () { return require("nativescript-pulltorefresh").PullToRefresh; });
require("rxjs/add/operator/map");
require("rxjs/add/operator/do");
var HomeListComponent = /** @class */ (function () {
    function HomeListComponent(http) {
        this.http = http;
        this.notices = {
            articles: []
        };
        this.temporalUrl = "";
        this.title = "hola";
        this.getNotices("https://newsapi.org/v1/articles?source=abc-news-au&sortBy=top&apiKey");
    }
    // https://newsapi.org/v1/articles?source=abc-news-au&sortBy=top&apiKey
    // metodo para consultar noticias
    HomeListComponent.prototype.getNotices = function (url, args) {
        var _this = this;
        this.temporalUrl = url;
        // this.notices = [];
        var headers = new http_1.Headers();
        // set headers here e.g.
        headers.append("Content-Type", "application/json");
        this.http.get(url + "=1b13ca21cd1742c78f34609b939503a2", { headers: headers }).map(function (res) { return res.json(); }).subscribe(function (res) {
            _this.notices = res;
            _this.title = res.source;
            args.object.refreshing = false;
            console.log(_this.notices.articles);
        }, function (error) {
            console.log(error);
        });
    };
    HomeListComponent = __decorate([
        core_1.Component({
            selector: 'home-list',
            //     template: ` <SegmentedBar>
            //     <SegmentedBarItem title="Hola">
            //     </SegmentedBarItem>
            //     <SegmentedBarItem title="Hola">
            //     </SegmentedBarItem>
            // </SegmentedBar>`
            templateUrl: "./home/home-list/home-list.html"
        }),
        __metadata("design:paramtypes", [http_1.Http])
    ], HomeListComponent);
    return HomeListComponent;
}());
exports.HomeListComponent = HomeListComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImhvbWUtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsc0NBQXdEO0FBR3hELDBFQUF3RTtBQUN4RSxrQ0FBZSxDQUFDLGVBQWUsRUFBQyxjQUFNLE9BQUEsT0FBTyxDQUFDLDRCQUE0QixDQUFDLENBQUMsYUFBYSxFQUFuRCxDQUFtRCxDQUFDLENBQUM7QUFDM0YsaUNBQStCO0FBQy9CLGdDQUE4QjtBQVk5QjtJQVFJLDJCQUFtQixJQUFVO1FBQVYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQVB0QixZQUFPLEdBQVE7WUFDbEIsUUFBUSxFQUFFLEVBQ1Q7U0FDSixDQUFDO1FBQ0ssZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFDekIsVUFBSyxHQUFHLE1BQU0sQ0FBQztRQUdsQixJQUFJLENBQUMsVUFBVSxDQUFDLHNFQUFzRSxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUdELHVFQUF1RTtJQUN2RSxpQ0FBaUM7SUFDakMsc0NBQVUsR0FBVixVQUFXLEdBQUcsRUFBQyxJQUFVO1FBQXpCLGlCQWNDO1FBYkcsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7UUFDdkIscUJBQXFCO1FBQ3JCLElBQUksT0FBTyxHQUFHLElBQUksY0FBTyxFQUFFLENBQUM7UUFDNUIsd0JBQXdCO1FBQ3hCLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUksR0FBRyxzQ0FBbUMsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBVixDQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQy9HLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ25CLEtBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztZQUNSLElBQUksQ0FBQyxNQUFPLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdkMsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBN0JRLGlCQUFpQjtRQVY3QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFdBQVc7WUFDekIsaUNBQWlDO1lBQ2pDLHNDQUFzQztZQUN0QywwQkFBMEI7WUFDMUIsc0NBQXNDO1lBQ3RDLDBCQUEwQjtZQUMxQixtQkFBbUI7WUFDZixXQUFXLEVBQUUsaUNBQWlDO1NBQ2pELENBQUM7eUNBUzJCLFdBQUk7T0FScEIsaUJBQWlCLENBK0I3QjtJQUFELHdCQUFDO0NBQUEsQUEvQkQsSUErQkM7QUEvQlksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEh0dHAsIEhlYWRlcnMsIFJlc3BvbnNlIH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcbmltcG9ydCB7IFB1bGxUb1JlZnJlc2ggfSBmcm9tIFwibmF0aXZlc2NyaXB0LXB1bGx0b3JlZnJlc2hcIjtcbmltcG9ydCB7IE9ic2VydmFibGUgYXMgUnhPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnlcIjtcbnJlZ2lzdGVyRWxlbWVudChcInB1bGxUb1JlZnJlc2hcIiwoKSA9PiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXB1bGx0b3JlZnJlc2hcIikuUHVsbFRvUmVmcmVzaCk7XG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9tYXBcIjtcbmltcG9ydCBcInJ4anMvYWRkL29wZXJhdG9yL2RvXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnaG9tZS1saXN0Jyxcbi8vICAgICB0ZW1wbGF0ZTogYCA8U2VnbWVudGVkQmFyPlxuLy8gICAgIDxTZWdtZW50ZWRCYXJJdGVtIHRpdGxlPVwiSG9sYVwiPlxuLy8gICAgIDwvU2VnbWVudGVkQmFySXRlbT5cbi8vICAgICA8U2VnbWVudGVkQmFySXRlbSB0aXRsZT1cIkhvbGFcIj5cbi8vICAgICA8L1NlZ21lbnRlZEJhckl0ZW0+XG4vLyA8L1NlZ21lbnRlZEJhcj5gXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9ob21lL2hvbWUtbGlzdC9ob21lLWxpc3QuaHRtbFwiXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVMaXN0Q29tcG9uZW50IHtcbiAgICBwdWJsaWMgbm90aWNlczogYW55ID0ge1xuICAgICAgICBhcnRpY2xlczogW1xuICAgICAgICBdXG4gICAgfTtcbiAgICBwdWJsaWMgdGVtcG9yYWxVcmw6IHN0cmluZyA9IFwiXCI7XG4gICAgcHVibGljIHRpdGxlID0gXCJob2xhXCI7XG5cbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgaHR0cDogSHR0cCkge1xuICAgICAgICB0aGlzLmdldE5vdGljZXMoXCJodHRwczovL25ld3NhcGkub3JnL3YxL2FydGljbGVzP3NvdXJjZT1hYmMtbmV3cy1hdSZzb3J0Qnk9dG9wJmFwaUtleVwiKTtcbiAgICB9XG5cblxuICAgIC8vIGh0dHBzOi8vbmV3c2FwaS5vcmcvdjEvYXJ0aWNsZXM/c291cmNlPWFiYy1uZXdzLWF1JnNvcnRCeT10b3AmYXBpS2V5XG4gICAgLy8gbWV0b2RvIHBhcmEgY29uc3VsdGFyIG5vdGljaWFzXG4gICAgZ2V0Tm90aWNlcyh1cmwsYXJncz86IGFueSkge1xuICAgICAgICB0aGlzLnRlbXBvcmFsVXJsID0gdXJsO1xuICAgICAgICAvLyB0aGlzLm5vdGljZXMgPSBbXTtcbiAgICAgICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycygpO1xuICAgICAgICAvLyBzZXQgaGVhZGVycyBoZXJlIGUuZy5cbiAgICAgICAgaGVhZGVycy5hcHBlbmQoXCJDb250ZW50LVR5cGVcIiwgXCJhcHBsaWNhdGlvbi9qc29uXCIpO1xuICAgICAgICB0aGlzLmh0dHAuZ2V0KGAke3VybH09MWIxM2NhMjFjZDE3NDJjNzhmMzQ2MDliOTM5NTAzYTJgLCB7IGhlYWRlcnM6IGhlYWRlcnMgfSkubWFwKHJlcyA9PiByZXMuanNvbigpKS5zdWJzY3JpYmUocmVzID0+IHtcbiAgICAgICAgICAgIHRoaXMubm90aWNlcyA9IHJlcztcbiAgICAgICAgICAgIHRoaXMudGl0bGUgPSByZXMuc291cmNlO1xuICAgICAgICAgICAgKDxQdWxsVG9SZWZyZXNoPmFyZ3Mub2JqZWN0KS5yZWZyZXNoaW5nID0gZmFsc2U7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLm5vdGljZXMuYXJ0aWNsZXMpO1xuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgIH0pXG4gICAgfVxuXG59XG4iXX0=